#pragma once
#include "IndexedTriangleList.h"
#include <DirectXMath.h>

class Cube
{
public:
	template<class V>
	static IndexedTriangleList<V> Make()
	{
		namespace dx = DirectX;

		constexpr float side = 1.0f / 2.0f;

		std::vector<dx::XMFLOAT3> vertices;
	
		vertices.emplace_back(-side, side, -side);
		vertices.emplace_back(side,side,side);
		vertices.emplace_back(side, side, -side);
		vertices.emplace_back(-side, -side, side);
		vertices.emplace_back(side, -side, side);
		vertices.emplace_back(-side, side,side);
		vertices.emplace_back(-side, -side, -side);
		vertices.emplace_back(-side, -side, side);
		vertices.emplace_back(side, -side, -side);
		vertices.emplace_back(-side, -side, side);
		vertices.emplace_back(-side, -side, -side);
		vertices.emplace_back(-side, side, -side);
		vertices.emplace_back(-side, side, side);
		vertices.emplace_back(-side, side, side);


		std::vector<V> verts(vertices.size());
		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return{
			std::move(verts),{
				
				0, 1, 2, 1, 3, 4, 
				5, 6, 7, 8, 9, 10, 
				2, 4, 8, 11, 8, 6, 
				0, 12, 1, 1, 13, 3, 
				5, 11, 6, 8, 4, 9, 
				2, 1, 4, 11, 2, 8
			}
		};
	}
};